Maintenance utilities for Docker
================================

- `dockergc.sh` - garbage collector for Docker. Cleans up unused images and
  volume directories.  
  **NB:** It does not work with Swarm. Local Docker engine only.
