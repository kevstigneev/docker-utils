#!/bin/bash
#
# Clean up unused Docker stuff.

fail() {
    exec >&2
    echo "Failure: $1"
    exit 1
}

gc_volumes() {
    echo "Remove unused anonymous volumes"

    docker volume ls -f 'dangling=true' -q \
        | grep '^[0-9a-f]\{64\}$' \
        | xargs -r docker volume rm
}

gc_images() {
    echo "Remove dangling images"

    docker images -f 'dangling=true' -q \
        | xargs -r docker rmi
}

# Remove stopped containers that looks like failed steps from Docker builds:
#  - named like "thirsty_mcnutty";
#  - have non-zero exit code.
gc_containers() {
    local grace_period="$1";  shift

    echo "Remove stopped intermediate containers from builds"

    threshold=$(date -u -Iseconds -d "-${grace_period}")
    docker ps -q -f status=exited \
        | xargs -r -L1 docker inspect --format '{{.Id}} {{.Name}} {{.State.FinishedAt}} {{.State.ExitCode}}' \
        | while read id name finishedAt exitCode; do
            [[ ${name}/ =~ ^/[a-z]+_[a-z]+/$ ]] || continue
            [[ ${finishedAt} < ${threshold} ]] || continue
            [[ ${exitCode} -ne 0 ]] || continue
            echo ${id}
        done \
        | xargs -r docker rm
}

set -e -o pipefail

grace_time="5 days"

case $1 in
    containers)
        gc_containers "${2:-${grace_time}}"
        ;;
    images)
        gc_images
        ;;
    volumes)
        gc_volumes
        ;;
    all)
        gc_containers "${grace_time}"
        gc_volumes
        gc_images
        ;;
    *)
        fail "Command not recognized"
        ;;
esac
