BINDIR = $$HOME/bin

all:

install: dockergc.sh
	for script in $^; do \
		targetname=`basename -s .sh $$script` ; \
		install -v -D -m 755 $$script $(BINDIR)/$$targetname ; \
	done

.PHONY: all install
